from aiogram.types.inline_keyboard import InlineKeyboardButton, InlineKeyboardMarkup

first_inline_buttons = InlineKeyboardMarkup(
    inline_keyboard=
    [
        [
            InlineKeyboardButton(text="Тык", callback_data="inline_1"),
            InlineKeyboardButton(text="И сюда тык", callback_data="inline_2"),
        ],
        [
            InlineKeyboardButton(text="Здесь котик", callback_data="inline_3",
                                 url="http://itd2.mycdn.me/image?id=839347759789&t=20&plc=WEB&tkn"
                                     "=*ZtArN_g5zNCYnaNVryBhilAyYcE"),
        ],
        [
            InlineKeyboardButton(text="Не тыкай", callback_data="inline_4"),
        ],
    ]
)

second_inline_buttons = InlineKeyboardMarkup(
    inline_keyboard=
    [
        [
            InlineKeyboardButton(text='Продолжаем!', callback_data='inline_5')
        ]
    ]
)
