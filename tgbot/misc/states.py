from aiogram.dispatcher.filters.state import State, StatesGroup


class GetContact(StatesGroup):
    phone_number = State()
    location = State()
