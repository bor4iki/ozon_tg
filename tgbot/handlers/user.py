import asyncio

from aiogram import Dispatcher
from aiogram.types import Message

from tgbot.services.ozon_postavka import main

from tgbot.keyboards.reply import first_buttons
from tgbot.services.get_logger import logger
from tgbot.services.ozon_stock import get_stock


async def user_start(message: Message):
    full_name = message.from_user.full_name
    logger().info(f'{full_name}/{message.from_user.id} start use bot')
    await message.answer("Бот для отcлеживания поставки на OZON",
                         reply_markup=first_buttons)


async def get_postavka(message: Message):
    full_name = message.from_user.full_name
    logger().info(f'{full_name}/{message.from_user.id} get_postavka')
    await message.answer("Идет обращение к серверу. Подождите...")
    # print(main())
    data_postavka: dict = main()

    for _, data in data_postavka.items():
        await message.answer(data)


async def show_tovar(message: Message):
    out = get_stock()
    # print(out)
    mess = ''
    for i, l in out['шт.'].items():
        mess += f'{i[0]}, {i[1]} - {l}\n'
        # print(mess)
    await message.answer(mess)


def register_user(dp: Dispatcher):
    dp.register_message_handler(user_start, commands=["start"], state="*")
    dp.register_message_handler(get_postavka, text=['Смотреть поставку'])
    dp.register_message_handler(show_tovar, text=['give_stock'])
