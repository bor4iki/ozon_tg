from aiogram import types, Dispatcher
from tgbot.services.get_logger import logger


async def user_contacts(message: types.Message):
    text = 'Привет еще раз!\n' \
           'Я не много расскажу о себе. Я занимаюсь программированием больше 3-х лет. Начал заниматься, как самоучка.' \
           ' Потом эти навыки начал использовать на своей работе. И по-тихоньку вышел на данный уровень. ' \
           'У меня в стеке есть десятки работ, которые до сих пор работают у моих клиентов.\n' \
           'Все боты/скрипты работают на сервере, который я сам настроил. Вам не нужно переживать, ' \
           'а как и где будет работать 😊 \n\n' \
           'Мои контактные данные:\n' \
           'Телегамм - @borisovbk\n' \
           'E-mail - borisovb@outlook.com\n' \
           'Бот напоминалка о днях рождениях - @jobBorisov_bot\n' \
           'Жду коммерческих предложений и взаимовыгодного сотрудничества!\n'
    logger().info(f'{message.from_user.full_name}/{message.from_user.id}/{message.from_user.username} '
                  f'- go in to /contacts')
    await message.answer(text)


def register_contacts(dp: Dispatcher):
    dp.register_message_handler(user_contacts, commands=["contacts"], state="*")
