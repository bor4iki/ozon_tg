import sqlite3
import requests

import pandas as pd

client_id = '753405'
key = 'c3b2eb44-e367-4d1f-a5f4-780066f85de8'


def get_request():
    headers = {"Client-Id": client_id,
               'Api-Key': key}
    data = {'limit': 1000,
            'offset': 0,
            'warehouse_type': "ALL"}
    url = 'https://api-seller.ozon.ru/v2/analytics/stock_on_warehouses'
    r = requests.post(url, headers=headers, json=data)

    output = r.json()['result']['rows']
    df = pd.DataFrame(output)
    return df


def add_new_order(database):
    with sqlite3.connect(database, check_same_thread=False) as conn:
        df = get_request()
        df.to_sql(name="Order", con=conn, if_exists='append', index=False)


def get_output_df(database):
    with sqlite3.connect(database, check_same_thread=False) as conn:
        df_output = pd.read_sql(sql='SELECT * FROM "Order"', con=conn)
        return df_output


def main():
    # db = r'C:\Users\Boris\Documents\Programming\ozon_tg\tgbot\services\bot_db.db'
    db_linux = r'/home/Bor/ozon_bot/ozon_tg/tgbot/services/bot_db.db'
    add_new_order(db_linux)


if __name__ == '__main__':
    print('Start script')
    main()
