import logging


def logger(name=__name__, path_name='log'):
    logging.basicConfig(
        level=logging.INFO,
        format=u'%(filename)s:%(lineno)d #%(levelname)-8s [%(asctime)s] - %(name)s - %(message)s',
        filename=f'{path_name}.log'
    )
    log = logging.getLogger(name)
    return log
