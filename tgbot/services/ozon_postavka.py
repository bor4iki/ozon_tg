import requests

from tgbot.config import load_config

# client_id_ooo = '1278597'
# key_ooo = '1283ddd8-2c5d-490d-9114-a9ce5c2aa53d'

config = load_config(".env")


def parse_json(supply_info, info_items):
    supply_order_id = f"id - {supply_info['supply_order_id']}\n"
    supply_order_number = f"Номер отправления - {supply_info['supply_order_number']}\n"
    created_at = f"Дата создания заявки - {supply_info['created_at']}\n"
    state = f"Статус заявки - {supply_info['state']}\n"
    local_timeslot = f"Дата и время отгрузки - {supply_info['local_timeslot']}\n\n"
    items = ''
    for i in info_items['items']:
        items += f"Артикул товара - {i['offer_id']}\n"
        items += f"Кол-во товара - {i['quantity']} шт.\n\n"
    warehouse = f"Адрес доставки - {supply_info['supply_warehouse']['address']}\n"
    name_warehouse = f"Название склада - {supply_info['supply_warehouse']['name']}\n"
    litres = f"Общий объем поставки - {supply_info['rounded_total_volume_in_litres']} литров, {supply_info['rounded_total_volume_in_litres'] / 1000} м3\n"
    driver_name = f"Водитель - {supply_info['vehicle_info']['driver_name']}"
    return supply_order_id + supply_order_number + created_at + state + local_timeslot + items + warehouse + name_warehouse + litres + driver_name + '\n\n----Конец заявки----\n'


def main():
    headers = {"Client-Id": config.ozon_api.ozon_id, 'Api-Key': config.ozon_api.ozon_key}
    data = {'page': 1,
            'page_size': 5,
            'states': ['READY_TO_SUPPLY']}
    url = 'https://api-seller.ozon.ru/v1/supply-order/list'
    r = requests.post(url, headers=headers, json=data)
    id_list = [x['supply_order_id'] for x in r.json()['supply_orders']]

    items_url = 'https://api-seller.ozon.ru/v1/supply-order/items'
    full_info_supply = 'https://api-seller.ozon.ru/v1/supply-order/get'
    data_dir = {}
    for x in id_list:
        supply_data = {'supply_order_id': x}
        supply_info = requests.post(full_info_supply, headers=headers, json=supply_data)
        items_data = {'page': 1, 'page_size': 5, 'supply_order_id': x}
        info_items = requests.post(items_url, headers=headers, json=items_data)
        data_dir[f'{x}'] = parse_json(supply_info.json(), info_items.json())
    return data_dir
