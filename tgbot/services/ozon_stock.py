import pandas as pd
import datetime

from tgbot.services.db_main import get_output_df


# from tgbot.services.db_main import SQLighter

def day_today():
    return datetime.date.today().day


def get_stock():
    # db = r'C:\Users\Boris\Documents\Programming\ozon_tg\tgbot\services\bot_db.db'
    db_linux = '/home/Bor/ozon_bot/ozon_tg/tgbot/services/bot_db.db'
    df_output = get_output_df(db_linux)
    df_output['date_order'] = df_output['date_order'].apply(lambda x: datetime.datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S'))
    now_df = df_output[df_output['date_order'].dt.day == day_today()]
    now_df = now_df[now_df['free_to_sell_amount'] > 0]
    yesterday_df = df_output[df_output['date_order'].dt.day == day_today() - datetime.timedelta(days=1).days]
    yesterday_df = yesterday_df[yesterday_df['free_to_sell_amount'] > 0]
    result = pd.merge(now_df, yesterday_df, on=["item_code", "warehouse_name"])
    result['diff'] = result['free_to_sell_amount_y'] - result['free_to_sell_amount_x']
    out: pd.DataFrame = result.groupby(['warehouse_name', 'item_code']).agg([('шт.', lambda x: x)])

    return out['diff'].to_dict()


if __name__ == '__main__':
    get_stock()
